mount -o rw,remount /vendor || mount -o rw,remount /system

targetfile="/vendor/etc/init/vendor.lineage.touch@1.0-service.judyln.rc"
ui_print "- Remove ${targetfile}"
rm "${targetfile}"

targetfile="/vendor/bin/hw/vendor.lineage.touch@1.0-service.judyln"
ui_print "- Remove ${targetfile}"
rm "${targetfile}"
rmdir /vendor/bin/hw || true

targetfile="/vendor/lib64/vendor.lineage.touch@1.0.so"
ui_print "- Remove ${targetfile}"
rm "${targetfile}"

mount -o ro,remount /vendor || mount -o ro,remount /system
