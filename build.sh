#!/usr/bin/env bash

rm -rvf release
mkdir release
for d in *; do
  module_prop="${d}/module.prop"
  if [ -f "${module_prop}" ]; then
    source "${module_prop}"
    release_zip="../release/${id}_${version}.zip"
    (
      cd "${d}"
      rm "${release_zip}"
      7z a "${release_zip}" *
    )
  fi
done
