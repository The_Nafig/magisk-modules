mount -o rw,remount /vendor || mount -o rw,remount /system

targetfile="/vendor/etc/init/android.hardware.light@2.0-service.judyln.rc"
ui_print "- Remove ${targetfile}"
rm "${targetfile}"

targetfile="/vendor/bin/hw/android.hardware.light@2.0-service.judyln"
ui_print "- Remove ${targetfile}"
rm "${targetfile}"
rmdir /vendor/bin/hw || true

mount -o ro,remount /vendor || mount -o ro,remount /system
