mount -o rw,remount /system || mount -o rw,remount /

targetfile="/etc/init/QuadDacPanel.rc"
ui_print "- Remove ${targetfile}"
rm "${targetfile}"

mount -o ro,remount /system || mount -o ro,remount /
