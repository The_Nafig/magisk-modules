##########################################################################################
#
# Magisk Module Installer Script
#
##########################################################################################
##########################################################################################
#
# Instructions:
#
# 1. Place your files into system folder (delete the placeholder file)
# 2. Fill in your module's info into module.prop
# 3. Configure and implement callbacks in this file
# 4. If you need boot scripts, add them into common/post-fs-data.sh or common/service.sh
# 5. Add your additional or modified system properties into common/system.prop
#
##########################################################################################

##########################################################################################
# Config Flags
##########################################################################################

# Set to true if you do *NOT* want Magisk to mount
# any files for you. Most modules would NOT want
# to set this flag to true
SKIPMOUNT=false

# Set to true if you need to load system.prop
PROPFILE=true

# Set to true if you need post-fs-data script
POSTFSDATA=true

# Set to true if you need late_start service script
LATESTARTSERVICE=true

##########################################################################################
# Replace list
##########################################################################################

# List all directories you want to directly replace in the system
# Check the documentations for more info why you would need this

# Construct your list in the following format
REPLACE="
"

##########################################################################################
#
# Function Callbacks
#
# The following functions will be called by the installation framework.
# You do not have the ability to modify update-binary, the only way you can customize
# installation is through implementing these functions.
#
# When running your callbacks, the installation framework will make sure the Magisk
# internal busybox path is *PREPENDED* to PATH, so all common commands shall exist.
# Also, it will make sure /data, /system, and /vendor is properly mounted.
#
##########################################################################################
##########################################################################################
#
# The installation framework will export some variables and functions.
# You should use these variables and functions for installation.
#
# ! DO NOT use any Magisk internal paths as those are NOT public API.
# ! DO NOT use other functions in util_functions.sh as they are NOT public API.
# ! Non public APIs are not guranteed to maintain compatibility between releases.
#
# Available variables:
#
# MAGISK_VER (string): the version string of current installed Magisk
# MAGISK_VER_CODE (int): the version code of current installed Magisk
# BOOTMODE (bool): true if the module is currently installing in Magisk Manager
# MODPATH (path): the path where your module files should be installed
# TMPDIR (path): a place where you can temporarily store files
# ZIPFILE (path): your module's installation zip
# ARCH (string): the architecture of the device. Value is either arm, arm64, x86, or x64
# IS64BIT (bool): true if $ARCH is either arm64 or x64
# API (int): the API level (Android version) of the device
#
# Availible functions:
#
# ui_print <msg>
#     print <msg> to console
#     Avoid using 'echo' as it will not display in custom recovery's console
#
# abort <msg>
#     print error message <msg> to console and terminate installation
#     Avoid using 'exit' as it will skip the termination cleanup steps
#
# set_perm <target> <owner> <group> <permission> [context]
#     if [context] is empty, it will default to "u:object_r:system_file:s0"
#     this function is a shorthand for the following commands
#       chown owner.group target
#       chmod permission target
#       chcon context target
#
# set_perm_recursive <directory> <owner> <group> <dirpermission> <filepermission> [context]
#     if [context] is empty, it will default to "u:object_r:system_file:s0"
#     for all files in <directory>, it will call:
#       set_perm file owner group filepermission context
#     for all directories in <directory> (including itself), it will call:
#       set_perm dir owner group dirpermission context
#
##########################################################################################
##########################################################################################
# If you need boot scripts, DO NOT use general boot scripts (post-fs-data.d/service.d)
# ONLY use module scripts as it respects the module status (remove/disable) and is
# guaranteed to maintain the same behavior in future Magisk releases.
# Enable boot scripts by setting the flags in the config section above.
##########################################################################################

# Set what you want to display when installing your module

# copy from Unity (Un)Install Utility Functions
run_addons() {
  local OPT=`getopt -o mhiuv -- "$@"` NAME PNAME
  eval set -- "$OPT"
  while true; do
    case "$1" in
      -m) NAME=main; shift;;
      -h) NAME=preinstall; PNAME="Preinstall"; shift;;
      -i) NAME=install; PNAME="Install"; shift;;
      -u) NAME=uninstall; PNAME="Uninstall"; shift;;
      -v) NAME=postuninstall; PNAME="Postuninstall"; shift;;
      --) shift; break;;
    esac
  done
  if [ "$(ls -A $TMPDIR/addon/*/$NAME.sh 2>/dev/null)" ]; then
    [ -z $PNAME ] || { ui_print " "; ui_print "- Running $PNAME Addons -"; }
    for i in $TMPDIR/addon/*/$NAME.sh; do
      ui_print "  Running $(echo $i | sed -r "s|$TMPDIR/addon/(.*)/$NAME.sh|\1|")..."
      . $i
    done
    [ -z $PNAME ] || { ui_print " "; ui_print "- `echo $PNAME`ing (cont) -"; }
  fi
}

print_modname() {
  ui_print "******************************************************"
  ui_print "*   Enable Nethunter HID gadget function - fasheng   *"
  ui_print "******************************************************"
}

# insert_section <file> <begin search string> <end search string> <content string>
insert_section() {
  if is_section_exists "$1" "$2" "$3"; then
    replace_section "$1" "$2" "$3" "$2\n$4\n$3"
  else
    echo -e "$2\n$4\n$3" >> "$1"
  fi
}

# is_section_exists <file> <begin search string> <end search string>
is_section_exists() {
  local begin endstr end;
  begin=$(grep -n "$2" $1 | head -n1 | cut -d: -f1);
  if [ "$begin" ]; then
    if [ "$3" == " " -o ! "$3" ]; then
      endstr='^[[:space:]]*$';
    else
      endstr="$3";
    fi;
    for end in $(grep -n "$endstr" $1 | cut -d: -f1) $last; do
      if [ "$end" ] && [ "$begin" -lt "$end" ]; then
        return;
      fi;
    done;
  fi;
  return 1;
}

# replace_section <file> <begin search string> <end search string> <replacement string>
# copy and fix last line issue from anykernel3/ak3_core.sh
replace_section() {
  local begin endstr last end;
  begin=$(grep -n "$2" $1 | head -n1 | cut -d: -f1);
  if [ "$begin" ]; then
    if [ "$3" == " " -o ! "$3" ]; then
      endstr='^[[:space:]]*$';
    else
      endstr="$3";
    fi;
    last=$(wc -l $1 | cut -d\  -f1);
    for end in $(grep -n "$endstr" $1 | cut -d: -f1) $last; do
      if [ "$end" ] && [ "$begin" -lt "$end" ]; then
        sed -i "${begin},${end}d" $1;
        if [ "$end" == "$last" ]; then
          echo >> $1;
          sed -i "${begin}s;^;${4}\n;" $1;
          sed -i '$d' $1;
        else
          sed -i "${begin}s;^;${4}\n;" $1;
        fi
        break;
      fi;
    done;
  fi;
}

# remount as read write
remount_rw() {
  # Remount as read write
  if mountpoint -q /vendor; then
    mount -o rw,remount /vendor
  elif mountpoint -q /system; then
    mount -o rw,remount /system
  elif mountpoint -q /; then
    mount -o rw,remount /
  fi

  if mountpoint -q /vendor/etc; then
    if is_overlay_mountpont /vendor/etc; then
      vendor_etc_dir=$(get_overlay_lowerdir /vendor/etc)
      ui_print "- Found overly mount on /vendor/etc, lowerdir is ${vendor_etc_dir}"
      if mountpoint -q /system; then
        mount -o rw,remount /system
      elif mountpoint -q /; then
        mount -o rw,remount /
      fi
    else
      mount -o rw,remount /vendor/etc
    fi
  fi
}

# remount as read only
remount_ro() {
  # Remount as read only
  if mountpoint -q /vendor; then
    mount -o ro,remount /vendor
  fi
  if mountpoint -q /system; then
    mount -o ro,remount /system
  fi
  if mountpoint -q /; then
    mount -o rw,remount /
  fi
}

# arg1: mount path
is_overlay_mountpont() {
  mount -t overlay | grep -q "on ${1} type overlay "
}

# arg1: mount path
get_mount_options() {
  mount | grep " on ${1} type " | sed "s/.*(\(.*\))$/\1/" | tr ',' '\n'
}

# arg1: mount path
get_overlay_lowerdir() {
  get_mount_options "${1}" | grep '^lowerdir=' | sed 's/^lowerdir=//' | sed 's/:.*$//'
}

# Copy/extract your module files into $MODPATH in on_install.

vendor_dir='/vendor'
vendor_etc_dir='/vendor/etc'
on_install() {
  # Unzip files
  ui_print " "
  ui_print "Unzipping files..."
  unzip -oq "$ZIPFILE" -d $TMPDIR 2>/dev/null

  # Preinstall Addons
  run_addons -h

  local hardware=$(getprop ro.hardware)
  local section_begin='### HID GADGET BEGIN'
  local section_end='### HID GADGET END'
  local targetfile=

  remount_rw

  # The following is the default implementation: extract $ZIPFILE/system to $MODPATH
  # Extend/change the logic to whatever you want
  local init_rc=
  ui_print "- Choose usb gadget type:"
  ui_print "  Vol+ = default"
  ui_print "  Vol- = older(Android Composite Gadget, kernel option CONFIG_USB_G_ANDROID=y)"
  if $VKSEL; then
    init_rc='init.nethunter.rc'
  else
    init_rc='init.nethunter-android-composite.rc'
  fi
  ui_print "- Choose ${init_rc}"

  targetfile="${vendor_etc_dir}/init/init.nethunter.rc"
  if [ -f "${targetfile}" ]; then
    ui_print "- Remove old ${targetfile}"
    rm "${targetfile}"
  fi
  targetfile="${vendor_etc_dir}/init/init.nethunter-android-composite.rc"
  if [ -f "${targetfile}" ]; then
    ui_print "- Remove old ${targetfile}"
    rm "${targetfile}"
  fi

  ui_print "- Extracting module files"
  unzip -o "$ZIPFILE" "${init_rc}" -d "${vendor_etc_dir}/init"
  unzip -o "$ZIPFILE" '*.bin' -d "${vendor_dir}"

  targetfile="${vendor_etc_dir}/init/${init_rc}"
  local gadget_path="/config/usb_gadget/g1/configs/$(ls /config/usb_gadget/g1/configs | head -n 1)"
  ui_print "- Fix usb gadget path to: ${gadget_path}"
  sed -i "s|/config/usb_gadget/g1/configs/.\.1|${gadget_path}|g" "${targetfile}"
  ui_print "- Fix permission ${targetfile}"
  set_perm "${targetfile}" 0 0 0644 u:object_r:system_file:s0

  targetfile="${vendor_dir}/ueventd.rc"
  ui_print "- Patch ${targetfile}"
  insert_section "${targetfile}" "${section_begin}" "${section_end}" "/dev/hidg* 0666 root root"

  remount_ro
}

# Only some special files require specific permissions
# This function will be called after on_install is done
# The default permissions should be good enough for most cases

set_permissions() {
  # The following is the default rule, DO NOT remove
  set_perm_recursive $MODPATH 0 0 0755 0644

  # Here are some examples:
  # set_perm_recursive  $MODPATH/system/lib       0     0       0755      0644
  # set_perm  $MODPATH/system/bin/app_process32   0     2000    0755      u:object_r:zygote_exec:s0
  # set_perm  $MODPATH/system/bin/dex2oat         0     2000    0755      u:object_r:dex2oat_exec:s0
  # set_perm  $MODPATH/system/lib/libart.so       0     0       0644
}

# You can add more functions to assist your custom script code
